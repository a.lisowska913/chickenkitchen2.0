﻿using ChickenKitchen.Data.Dishes;
using ChickenKitchen.Data.ClientData.ListsFromClientFile;

namespace TestKitchen
{
    internal class MakeListsTests
    {
        [Test]
        public void MakeDishListTest()
        {
            //Given
            string[] restaurantDishesIngredientsFile = { "Tuna Cake: Tuna, Chocolate, Youth Sauce" };
            List<string> expected = new List<string> { "Tuna Cake" };

            //When
            List<string> actual = new MakeListsFromtDishesIngredientsFile(restaurantDishesIngredientsFile)
                .MakeDishList();

            //Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void MakeDishIngredientsListTest()
        {
            //Given
            string[] restaurantDishesIngredientsFile = { "Tuna Cake: Tuna, Chocolate, Youth Sauce" };
            Dictionary<string, string[]> expected = new Dictionary<string, string[]>();
            expected.Add("Tuna Cake", new string[] { "Tuna", "Chocolate", "Youth Sauce" });

            //When
            Dictionary<string, string[]> actual = new MakeListsFromtDishesIngredientsFile(restaurantDishesIngredientsFile)
                .MakeDishIngredientsDict();

            //Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void MakeBaseIngredientsListTest()
        {
            //Given
            string[] baseIngredientsFile = { "Tuna: 10", "Chocolate: 11" };
            List<string> expected = new List<string> { "Tuna", "Chocolate" };

            //When
            List<string> actual = new MakeListsFromBasicIngredientsFile(baseIngredientsFile)
                .MakeBaseIngredientsList();

            //Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void MakeBaseIngredientsPriceListTest()
        {
            //Given
            string[] baseIngredientsFile = { "Tuna: 10", "Chocolate: 11" };
            List<int> expected = new List<int> { 10, 11 };

            //When
            List<int> actual = new MakeListsFromBasicIngredientsFile(baseIngredientsFile)
                .MakeBaseIngredientsPriceList();

            //Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void MakeClientListTest()
        {
            //Given
            string[] clientAllergiesFile = { "Anna Mirage: Tuna : 10", "Bernard Unfortunate: Potatoes, Chockolate : 100" };
            List<string> expected = new List<string> { "Anna Mirage", "Bernard Unfortunate" };

            //When
            List<string> actual = new MakeListFromClientFile(clientAllergiesFile)
                .MakeClientList();

            //Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void MakeClientAllergiesListOfArrayTest()
        {
            //Given
            string[] clientAllergiesFile = { "Anna Mirage: Tuna : 10", "Bernard Unfortunate: Potatoes, Chockolate : 100" };
            Dictionary<string, string[]> expected = new Dictionary<string, string[]>();
            expected.Add("Anna Mirage", new string[] { "Tuna" });
            expected.Add("Bernard Unfortunate", new string[] { "Potatoes", "Chockolate" });

            //When
            Dictionary<string, string[]> actual = new MakeListFromClientFile(clientAllergiesFile)
                .MakeClientAllergiesDict();

            //Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void MakeClientFundsTest()
        {
            //Given
            string[] clientAllergiesFile = { "Anna Mirage: Tuna : 10", "Bernard Unfortunate: Potatoes, Chockolate : 100" };
            List<int> expected = new List<int> { 10, 100 };

            //When
            List<int> actual = new MakeListFromClientFile(clientAllergiesFile)
                .MakeClientFundsList();

            //Then
            Assert.AreEqual(expected, actual);
        }
    }
}
