﻿using ChickenKitchen.Data.Dishes;
using ChickenKitchen.Data.Dishes.BasicIngredients;

namespace TestKitchen
{
    internal class AgregateIngredientsTests
    {
        [Test]
        public void AgregateSingleTest()    //testuje agregowanie pojedynczego elementa
        {
            //Given
            BasicIngredient[] baseIngredientsList = new BasicIngredient[]
            {
                new BasicIngredient ("Tuna", 0),
                new BasicIngredient ("Ketchup", 0),
                new BasicIngredient ("Potatoes", 0),
            };
            Dictionary<string, string[]> dishIngredientsList = new Dictionary<string, string[]>();
                dishIngredientsList.Add("Irish Fish", new string[] { "Tuna", "Fries", "Smashed Potatoes" });
                dishIngredientsList.Add("Fries", new string[] { "Potatoes", "Ketchup" });
                dishIngredientsList.Add("Smashed Potatoes", new string[] { "Potatoes" });

            List<string> dishList = new List<string> { "Irish Fish", "Fries", "Smashed Potatoes" };

            string dishName = "Irish Fish";

            string[] expected = { "Tuna", "Potatoes", "Ketchup", "Potatoes" };

            //When
            string[] actual = new CreateDishIngredientsDict()
                .Convert(dishName, baseIngredientsList, dishIngredientsList, dishList);

            //Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void DishToIngredientsTest()     //testuje agregowanie wszystkich elementów z listy
        {
            //Given
            BasicIngredient[] baseIngredientsList = new BasicIngredient[]
            {
                new BasicIngredient ("Tuna", 0),
                new BasicIngredient ("Lemon", 0),
                new BasicIngredient ("Water", 0),
                new BasicIngredient ("Tomatoes", 0),
                new BasicIngredient ("Vinegar", 0),
                new BasicIngredient ("Chockolate", 0)
            };

            Dictionary<string, string[]> dishIngredientsList = new Dictionary<string, string[]>();
                dishIngredientsList.Add("Fish in Water", new string[] { "Tuna", "Omega Sauce", "Ruby Salad" });
                dishIngredientsList.Add("Omega Sauce", new string[] { "Lemon", "Water" });
                dishIngredientsList.Add("Ruby Salad", new string[] { "Tomatoes", "Vinegar", "Chockolate" });

            List<string> dishList = new List<string> { "Fish in Water", "Omega Sauce", "Ruby Salad" };

            Dictionary<string, string[]> expected = new Dictionary<string, string[]>();
                expected.Add("Fish in Water", new string[] { "Tuna", "Lemon", "Water", "Tomatoes", "Vinegar", "Chockolate" });
                expected.Add("Omega Sauce", new string[] { "Lemon", "Water" });
                expected.Add("Ruby Salad", new string[] { "Tomatoes", "Vinegar", "Chockolate" });

            //When
            Dictionary<string, string[]> actual = new CreateDishIngredientsDict()
                .DishToIngredients(baseIngredientsList, dishIngredientsList, dishList);

            //Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void AgregateComplexDishIngredientsListTest()
        {
            //Given
            BasicIngredient[] baseIngredientsList = new BasicIngredient[]
            {
                new BasicIngredient ("Tuna", 0),
                new BasicIngredient ("Lemon", 0),
                new BasicIngredient ("Water", 0),
                new BasicIngredient ("Tomatoes", 0),
                new BasicIngredient ("Vinegar", 0),
                new BasicIngredient ("Chockolate", 0),
                new BasicIngredient ("Chicken", 0),
                new BasicIngredient ("Asparagus", 0),
                new BasicIngredient ("Milk", 0),
                new BasicIngredient ("Honey", 0),
                new BasicIngredient ("Pickles", 0),
                new BasicIngredient ("Feta", 0),
                new BasicIngredient ("Paprika", 0),
                new BasicIngredient ("Garlic", 0),
                new BasicIngredient ("Potatoes", 0),
            };

            Dictionary<string, string[]> dishIngredientsList = new Dictionary<string, string[]>();
                dishIngredientsList.Add("Emperor Chicken", new string[] { "Fat Cat Chicken", "Spicy Sauce", "Tuna Cake" });
                dishIngredientsList.Add("Fat Cat Chicken", new string[] { "Princess Chicken", "Youth Sauce", "Fries", "Dimond Salad" });
                dishIngredientsList.Add("Spicy Sauce", new string[] { "Paprika", "Garlic", "Water" });
                dishIngredientsList.Add("Tuna Cake", new string[] { "Tuna", "Chockolate", "Youth Sauce" });
                dishIngredientsList.Add("Youth Sauce", new string[] { "Asparagus", "Milk", "Honey" });
                dishIngredientsList.Add("Dimond Salad", new string[] { "Tomatoes", "Pickles", "Feta" });
                dishIngredientsList.Add("Princess Chicken", new string[] { "Chicken", "Youth Sauce" });
                dishIngredientsList.Add("Fries", new string[] { "Potatoes" });

            List<string> dishList = new List<string>
                { "Emperor Chicken", "Fat Cat Chicken", "Spicy Sauce", "Tuna Cake", "Youth Sauce", "Dimond Salad", "Princess Chicken", "Fries" };

            Dictionary<string, string[]> expected = new Dictionary<string, string[]>();
                expected.Add("Emperor Chicken", new string[] { "Chicken", "Asparagus", "Milk", "Honey", "Asparagus", "Milk", "Honey", "Potatoes", "Tomatoes", "Pickles", "Feta",
                        "Paprika", "Garlic", "Water", "Tuna", "Chockolate", "Asparagus", "Milk", "Honey" });
                expected.Add("Fat Cat Chicken", new string[] { "Chicken", "Asparagus", "Milk", "Honey", "Asparagus", "Milk", "Honey", "Potatoes", "Tomatoes", "Pickles", "Feta" });
                expected.Add("Spicy Sauce", new string[] { "Paprika", "Garlic", "Water" });
                expected.Add("Tuna Cake", new string[] { "Tuna", "Chockolate", "Asparagus", "Milk", "Honey" });
                expected.Add("Youth Sauce", new string[] { "Asparagus", "Milk", "Honey" });
                expected.Add("Dimond Salad", new string[] { "Tomatoes", "Pickles", "Feta" });
                expected.Add("Princess Chicken", new string[] { "Chicken", "Asparagus", "Milk", "Honey" });
                expected.Add("Fries", new string[] { "Potatoes" });

            //When
            Dictionary<string, string[]> actual = new CreateDishIngredientsDict()
                .DishToIngredients(baseIngredientsList, dishIngredientsList, dishList);

            //Then
            Assert.AreEqual(expected, actual);
        }
    }
}
