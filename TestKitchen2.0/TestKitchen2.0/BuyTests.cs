﻿using ChickenKitchen.Main.Operations;
using ChickenKitchen.Data;
using ChickenKitchen.Data.ClientData.ClientClassOperations;
using ChickenKitchen.Data.Dishes;
using ChickenKitchen.Data.Dishes.BasicIngredients;

namespace TestKitchen
{
    internal class BuyTests
    {
        [Test]
        public void BuySimpleDishNonAllergicClient()
        {
            //Given
            Dish[] dishList = new Dish[] { new Dish("Fries", new string[] { "Potatoes" }, new string[] { "Potatoes" }, 10) };
            Client[] clientList = new Client[] { new Client ("Julie Mirage", new string[] { "none" }, 100) };
            
            RestaurantRepo agregatedLists = new RestaurantRepoFactory()
                .CreateRestaurantRepo(dishList, new BasicIngredient[] { }, clientList);

            string clientName = "Julie Mirage";
            string clientOrder = "Fries";
            
            string expected = "none";

            //When
            string actual = new Order().CheckAllergies(agregatedLists, clientName, clientOrder);

            //Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void BuySimpleDishAllergicClient()
        {
            //Given
            Dish[] dishList = new Dish[] { new Dish("Fries", new string[] { "Potatoes" }, new string[] { "Potatoes" }, 10) };
            Client[] clientList = new Client[] { new Client("Julie Mirage", new string[] { "Potatoes" }, 100) };

            RestaurantRepo agregatedLists = new RestaurantRepoFactory()
                .CreateRestaurantRepo(dishList, new BasicIngredient[] { }, clientList);

            string clientName = "Julie Mirage";
            string clientOrder = "Fries";

            string expected = "Potatoes";

            //When
            string actual = new Order().CheckAllergies(agregatedLists, clientName, clientOrder);

            //Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void BuyDishNonAllergicClient()
        {
            //Given
            Dish[] dishList = new Dish[]
            {
                new Dish("Fish in Water", new string[] { "Tuna", "Omega Sauce", "Ruby Salad" }, new string[] { "Tuna", "Lemon", "Water", "Tomatoes", "Vinegar", "Chockolate" }, 10),
                new Dish("Omega Sauce", new string[] { "Lemon", "Water" }, new string[] { "Lemon", "Water" }, 10),
                new Dish("Ruby Salad", new string[] { "Tomatoes", "Vinegar", "Chockolate" }, new string[] { "Tomatoes", "Vinegar", "Chockolate" }, 10),
            };
            Client[] clientList = new Client[] { new Client("Julie Mirage", new string[] { "Soy" }, 100) };

            RestaurantRepo agregatedLists = new RestaurantRepoFactory()
                .CreateRestaurantRepo(dishList, new BasicIngredient[] { }, clientList);

            string clientName = "Julie Mirage";
            string clientOrder = "Fish in Water";

            string expected = "none";

            //When
            string actual = new Order().CheckAllergies(agregatedLists, clientName, clientOrder);

            //Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void BuyDishAllergicClient()
        {
            //Given
            Dish[] dishList = new Dish[]
            {
                new Dish("Fish in Water", new string[] { "Tuna", "Omega Sauce", "Ruby Salad" }, new string[] { "Tuna", "Lemon", "Water", "Tomatoes", "Vinegar", "Chockolate" }, 10),
                new Dish("Omega Sauce", new string[] { "Lemon", "Water" }, new string[] { "Lemon", "Water" }, 10),
                new Dish("Ruby Salad", new string[] { "Tomatoes", "Vinegar", "Chockolate" }, new string[] { "Tomatoes", "Vinegar", "Chockolate" }, 10),
            };
            Client[] clientList = new Client[] { new Client("Elon Carousel", new string[] { "Vinegar" }, 100) };

            RestaurantRepo agregatedLists = new RestaurantRepoFactory()
                .CreateRestaurantRepo(dishList, new BasicIngredient[] { }, clientList);

            string clientName = "Elon Carousel";
            string clientOrder = "Fish in Water";

            string expected = "Vinegar";

            //When
            string actual = new Order().CheckAllergies(agregatedLists, clientName, clientOrder);

            //Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void BuyComplexDishNonAllergicClient()
        {
            //Given
            Dish[] dishList = new Dish[]
            {
                new Dish("Emperor Chicken", new string[] { "Princess Chicken" }, new string[] { "Tuna", "Lemon", "Water", "Tomatoes", "Vinegar", "Chockolate", "Chicken", "Asparagus", "Milk", "Honey",
                    "Asparagus", "Milk", "Honey", "Tomatoes", "Pickles", "Feta", "Paprika", "Gralic", "Water", "Tuna", "Chockolate", "Asparagus", "Milk", "Honey",
                    "Asparagus", "Milk", "Honey", "Tomatoes", "Pickles", "Feta" }, 1000),
            };
            Client[] clientList = new Client[] { new Client("Julie Mirage", new string[] { "Soy" }, 100) };

            RestaurantRepo agregatedLists = new RestaurantRepoFactory()
                .CreateRestaurantRepo(dishList, new BasicIngredient[] { }, clientList);

            string clientName = "Julie Mirage";
            string clientOrder = "Emperor Chicken";

            string expected = "none";

            //When
            string actual = new Order().CheckAllergies(agregatedLists, clientName, clientOrder);

            //Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void BuyComplexDishAllergicClient()
        {
            //Given
            Dish[] dishList = new Dish[]
            {
                new Dish("Emperor Chicken", new string[] { "Princess Chicken" }, new string[] { "Tuna", "Lemon", "Water", "Tomatoes", "Vinegar", "Chockolate", "Chicken", "Asparagus", "Milk", "Honey",
                    "Asparagus", "Milk", "Honey", "Tomatoes", "Pickles", "Feta", "Paprika", "Gralic", "Water", "Tuna", "Chockolate", "Asparagus", "Milk", "Honey",
                    "Asparagus", "Milk", "Honey", "Tomatoes", "Pickles", "Feta" }, 1000),
            };
            Client[] clientList = new Client[] { new Client("Bernard Unfortunate", new string[] { "Tomatoes" }, 100) };

            RestaurantRepo agregatedLists = new RestaurantRepoFactory()
                .CreateRestaurantRepo(dishList, new BasicIngredient[] { }, clientList);

            string clientName = "Bernard Unfortunate";
            string clientOrder = "Emperor Chicken";

            string expected = "Tomatoes";

            //When
            string actual = new Order().CheckAllergies(agregatedLists, clientName, clientOrder);

            //Then
            Assert.AreEqual(expected, actual);
        }
    }
}
