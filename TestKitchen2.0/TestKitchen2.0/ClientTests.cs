﻿using ChickenKitchen.Data.ClientData.ClientClassOperations;

namespace TestKitchen
{
    internal class ClientTests
    {
        [Test]
        public void ClientNameTest()
        {
            //Given
            string clientName = "Julie Mirage";
            string[] clientAllergies = new string[] { "Soy", "Popcorn" };
            int clientFunds = 100;
            Client client = new Client("Julie Mirage", new string[] { "Soy", "Popcorn" }, 100);
            string expected = "Julie Mirage";

            //When
            string actual = new Client(clientName, clientAllergies, clientFunds).clientName;

            //Then
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void ClientAllergiesTest()
        {
            //Given
            string clientName = "Julie Mirage";
            string[] clientAllergies = new string[] { "Soy", "Popcorn" };
            int clientFunds = 100;

            Client client = new Client("Julie Mirage", new string[] { "Soy", "Popcorn" }, 100);
            string[] expected = new string[] { "Soy", "Popcorn" };

            //When
            string[] actual = new Client(clientName, clientAllergies, clientFunds).clientAllergies;

            //Then
            Assert.AreEqual(expected, actual);
        }
        [Test]
        public void ClientFundsTest()
        {
            //Given
            string clientName = "Julie Mirage";
            string[] clientAllergies = new string[] { "Soy", "Popcorn" };
            int clientFunds = 123;

            Client client = new Client("Julie Mirage", new string[] { "Soy", "Popcorn" }, 123);
            int expected = 123;

            //When
            int actual = new Client(clientName, clientAllergies, clientFunds).clientFunds;

            //Then
            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void CreateClientTest()
        {
            //Given
            string clientName = "Julie Mirage";
            string[] clientAllergies = new string[] { "Soy", "Popcorn" };
            int clientFunds = 100;
            Client expected = new Client("Julie Mirage", new string[] { "Soy", "Popcorn" }, 100);

            //When
            Client actual = new Client(clientName, clientAllergies, clientFunds);

            //Then
            actual.Equals(expected);
        }
        [Test]
        public void CreateClientListTest()
        {
            //Given
            string[] clientList = new string[] { "Julie Mirage", "Elon Carousel" };
            Dictionary<string, string[]> clientAllergiesDict = new Dictionary<string, string[]>();
            clientAllergiesDict.Add("Julie Mirage", new string[] { "Soy", "Popcorn" });
            clientAllergiesDict.Add("Elon Carousel", new string[] { "Vinegar"});
            int[] clientFundsList = new int[] { 100, 34 };

            List<Client> expected = new List<Client>
            {
                new Client("Julie Mirage", new string[] { "Soy", "Popcorn" }, 100),
                new Client("Elon Carousel", new string[] { "Vinegar" }, 34),
            };

            //When
            List<Client> actual = new CreateClientList().CreateListOfClients(clientList, clientAllergiesDict, clientFundsList);

            //Then
            actual.Equals(expected);
        }
    }
}
