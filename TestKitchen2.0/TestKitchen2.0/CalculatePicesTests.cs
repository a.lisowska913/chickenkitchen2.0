﻿using ChickenKitchen.Data.Dishes;
using ChickenKitchen.Data.Dishes.BasicIngredients;

namespace TestKitchen
{
    internal class CalculatePicesTests
    {
        [Test]
        public void CalculateDishPricesTest()
        {
            //Given
            BasicIngredient[] baseIngredientsList = new BasicIngredient[]
            {
                new BasicIngredient ("Chockolate", 10),
                new BasicIngredient ("Tuna", 11)
            };

            Dictionary<string, string[]> dishToIngredientsDict = new Dictionary<string, string[]>();
            dishToIngredientsDict.Add("Tuna Cake", new string[] { "Chockolate", "Tuna", "Tuna" });
            dishToIngredientsDict.Add("Tuna with Tuna", new string[] { "Tuna", "Tuna" });

            Dictionary<string, int> expected = new Dictionary<string, int>();
            expected.Add("Tuna Cake", 32);
            expected.Add("Tuna with Tuna", 22);

            //When
            Dictionary<string, int> actual = new CreateDishPriceList().Create(baseIngredientsList, dishToIngredientsDict);

            //Then
            Assert.AreEqual(expected, actual);
        }
    }
}
