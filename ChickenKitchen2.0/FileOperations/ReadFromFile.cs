﻿using System;

namespace ChickenKitchen.FileOperations
{
    public class ReadFromFile
    {
        public string MakeRelativePath(string path)                                                                         //Make relative path to file
        {
            Uri fullPath = new Uri(path);                                                                                   
            Uri relRoot = new Uri
                (@"C:\Users\Admin\source\repos\ChickenKitchen2.0\ChickenKitchen2.0\", UriKind.Absolute);

            string relPath = relRoot.MakeRelativeUri(fullPath).ToString();
            return relPath;
        }

        public string[] ReadFromBaseIngredientsFile()
        {
           return System.IO.File.ReadAllLines(MakeRelativePath("C:\\Users\\Admin\\source\\repos\\ChickenKitchen2.0\\ChickenKitchen2.0\\BaseIngredients.txt"));
        }

        public string[] ReadFromClientsAllergiesFile()
        {
            return System.IO.File.ReadAllLines(MakeRelativePath("C:\\Users\\Admin\\source\\repos\\ChickenKitchen2.0\\ChickenKitchen2.0\\RegularClientsAllergies.txt"));
        }

        public string[] ReadFromRestaurantDishesIngredientsFile()
        {
            return System.IO.File.ReadAllLines(MakeRelativePath("C:\\Users\\Admin\\source\\repos\\ChickenKitchen2.0\\ChickenKitchen2.0\\RestaurantDishesIngredients.txt"));
        }

    }
}
