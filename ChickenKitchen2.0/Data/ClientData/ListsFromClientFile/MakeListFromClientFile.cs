﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace ChickenKitchen.Data.ClientData.ListsFromClientFile
{
    public class MakeListFromClientFile
    {
        string[] clientAllergiesFile;
        public MakeListFromClientFile(string[] clientAllergiesFile)
        {
            this.clientAllergiesFile = clientAllergiesFile;
        }
        public List<string> MakeClientList()
        {
            Regex regex = new Regex(@"(\D*): (\D*) : (\d+)");
            List<string> clientList = new List<string>();
            foreach (var client in clientAllergiesFile)
                clientList.Add(regex.Match(client).Groups[1].Value);

            return clientList;
        }
        public Dictionary<string, string[]> MakeClientAllergiesDict()
        {
            Regex regex = new Regex(@"(\D*): (\D*) : (\d+)");
            Dictionary<string, string[]> clientAlergiesList = new Dictionary<string, string[]>();

            foreach (var client in clientAllergiesFile)
            {
                string[] allergies = regex.Match(client).Groups[2].Value.Split(',');
                for (int i = 0; i < allergies.Length; i++)
                    allergies[i] = allergies[i].Trim();

                clientAlergiesList.Add(regex.Match(client).Groups[1].Value, allergies);
            }
            return clientAlergiesList;
        }
        public List<int> MakeClientFundsList()
        {
            Regex regex = new Regex(@"(\D*): (\D*) : (\d+)");
            List<int> clientFundsList = new List<int>();
            foreach (var client in clientAllergiesFile)
                clientFundsList.Add(Int32.Parse(regex.Match(client).Groups[3].Value));
            
            return clientFundsList;
        }
    }
}
