﻿using ChickenKitchen.FileOperations;

namespace ChickenKitchen.Data.ClientData.ListsFromClientFile
{
    public class MakeListFromClientFileFactory
    {
        public MakeListFromClientFile Create()
        {
            ReadFromFile readFromFile = new ReadFromFile();
            string[] clientAllergiesFile;
            clientAllergiesFile = readFromFile.ReadFromClientsAllergiesFile();
            return new MakeListFromClientFile(clientAllergiesFile);
        }
    }
}
