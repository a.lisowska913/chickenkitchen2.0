﻿
namespace ChickenKitchen.Data.ClientData.ClientClassOperations
{
    public class Client
    {
        public string clientName { get; set; }
        public string[] clientAllergies { get; set; }
        public int clientFunds { get; set; }

        public Client(string clientName, string[] clientAllergies, int clientFunds)
        {
            this.clientName = clientName;
            this.clientAllergies = clientAllergies;
            this.clientFunds = clientFunds;
        }
    }
}
