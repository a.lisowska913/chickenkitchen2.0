﻿using System.Collections.Generic;
using ChickenKitchen.Data.ClientData.ListsFromClientFile;

namespace ChickenKitchen.Data.ClientData.ClientClassOperations
{
    public class CreateClientList
    {
        public List<Client> Create()
        {
            MakeListFromClientFile makeListFromClientFile = new MakeListFromClientFileFactory().Create();
            string[] clientList = makeListFromClientFile.MakeClientList().ToArray();
            Dictionary<string, string[]> clientAllergiesDict = makeListFromClientFile.MakeClientAllergiesDict();
            int[] clientFundsList = makeListFromClientFile.MakeClientFundsList().ToArray();

            List<Client> clientDataList = CreateListOfClients(clientList, clientAllergiesDict, clientFundsList);
            return clientDataList;
        }
        public List<Client> CreateListOfClients(string[] clientList, Dictionary<string, string[]> clientAllergiesDict, int[] clientFundsList)
        {
            int iterator = 0;
            List<Client> clientDataList = new List<Client>();
            foreach (var client in clientList)
            {
                Client currentClient = new Client(client, clientAllergiesDict[client], clientFundsList[iterator]);
                clientDataList.Add(currentClient);
                iterator++;
            }
            return clientDataList;
        }
    }
}
