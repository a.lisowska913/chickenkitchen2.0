﻿using System.Collections.Generic;
using System.Linq;
using ChickenKitchen.Data.Dishes.BasicIngredients;

namespace ChickenKitchen.Data.Dishes
{
    public class CreateDishDataList
    {
        public List<Dish> CreateList(BasicIngredient[] basicIngredientsList)
        {
            MakeListsFromtDishesIngredientsFile makeListsDishesIngredientsFromFiles = new MakeListsFromtDishesIngredientsFileFactory().Create();
            string[] dishList = makeListsDishesIngredientsFromFiles.MakeDishList().ToArray();
            Dictionary<string, string[]> dishIngredientsDict = makeListsDishesIngredientsFromFiles.MakeDishIngredientsDict();
            Dictionary<string, string[]> dishBasicIngredientsDict = 
                new CreateDishIngredientsDict().DishToIngredients(basicIngredientsList, dishIngredientsDict, dishList.ToList());
            Dictionary < string, int> dishPriceDict = new CreateDishPriceList().Create(basicIngredientsList, dishBasicIngredientsDict);

            return Create(dishList, dishBasicIngredientsDict, dishPriceDict);
        }

        public List<Dish> Create(string[] dishList,
            Dictionary<string, string[]> dishBasicIngredientsDict,
            Dictionary<string, int> dishPriceDict)
        {
            List<Dish> dishDataList = new List<Dish>();
            foreach (string dish in dishList)
            {
                dishDataList.Add(new Dish(dish, dishBasicIngredientsDict[dish], dishBasicIngredientsDict[dish], dishPriceDict[dish]));
            }
            return dishDataList;
        }

    }
}
