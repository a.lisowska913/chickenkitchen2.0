﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChickenKitchen.Data.Dishes
{
    public class Dish
    {
        public string dishName { get; set; }
        public string[] dishIngredients { get; set; }
        public string[] disBasicIngredients { get; set; }
        public int dishPrice { get; set; }

        public Dish(string dishName, string[] dishIngredients, string[] disBasicIngredients, int dishPrice)
        {
            this.dishName = dishName;
            this.dishIngredients = dishIngredients;
            this.disBasicIngredients = disBasicIngredients;
            this.dishPrice = dishPrice;
        }
    }
}
