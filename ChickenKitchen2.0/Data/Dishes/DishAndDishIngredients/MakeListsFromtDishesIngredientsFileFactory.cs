﻿using ChickenKitchen.FileOperations;

namespace ChickenKitchen.Data.Dishes
{
    public class MakeListsFromtDishesIngredientsFileFactory
    {
        public MakeListsFromtDishesIngredientsFile Create()
        {
            ReadFromFile readFromFile = new ReadFromFile();
            string[] restaurantDishesIngredientsFile;
            restaurantDishesIngredientsFile = readFromFile.ReadFromRestaurantDishesIngredientsFile();
            return new MakeListsFromtDishesIngredientsFile(restaurantDishesIngredientsFile);
        }
    }
}
