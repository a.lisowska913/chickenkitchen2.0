﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace ChickenKitchen.Data.Dishes
{
    public class MakeListsFromtDishesIngredientsFile
    {
        string[] restaurantDishesIngredientsFile;

        public MakeListsFromtDishesIngredientsFile(string[] restaurantDishesIngredientsFile)
        {
            this.restaurantDishesIngredientsFile = restaurantDishesIngredientsFile;
        }

        public List<string> MakeDishList()
        {
            Regex regex = new Regex(@"(\D*): (\D*)");
            List<string> dishList = new List<string>();
            foreach (string dish in restaurantDishesIngredientsFile)
                dishList.Add(regex.Match(dish).Groups[1].Value);

            return dishList;
        }

        public Dictionary<string, string[]> MakeDishIngredientsDict()
        {
            Regex regex = new Regex(@"(\D*): (\D*)");
            Dictionary<string, string[]> dishIngredientsList = new Dictionary<string, string[]>();

            foreach (string dish in restaurantDishesIngredientsFile)
            {
                string[] currentDishIngridientsList = (regex.Match(dish).Groups[2].Value).Split(',');
                for (int i = 0; i < currentDishIngridientsList.Length; i++)
                    currentDishIngridientsList[i] = currentDishIngridientsList[i].Trim();

                dishIngredientsList.Add(regex.Match(dish).Groups[1].Value, currentDishIngridientsList);
            }
            return dishIngredientsList;
        }
    }
}
