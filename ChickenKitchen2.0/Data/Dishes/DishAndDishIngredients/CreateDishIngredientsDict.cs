﻿using System.Collections.Generic;
using ChickenKitchen.Data.Dishes.BasicIngredients;

namespace ChickenKitchen.Data.Dishes
{
    public class CreateDishIngredientsDict
    {
        public Dictionary<string, string[]> DishToIngredients(BasicIngredient[] baseIngredientsList, Dictionary<string, string[]> dishIngredientsDict, List<string> dishList)
        {
            Dictionary<string, string[]> dishToIngredientsDict = new Dictionary<string, string[]>();
            foreach (string dish in dishList)
                dishToIngredientsDict.Add(dish, Convert(dish, baseIngredientsList, dishIngredientsDict, dishList));

            return dishToIngredientsDict;
        }

        public string[] Convert(string dishName,
            BasicIngredient[] baseIngredientsList,
            Dictionary<string, string[]> dishIngredientsDict,
            List<string> dishList)                                                                                      //converts dish to its basic ingredients
        {
            List<string> dishBasicIngredientsList = new List<string>();
            foreach (var dish in dishList)
            {
                if (dishName == dish)                                                                                   //jeśli danie na liście jest podanym daniem
                {
                    foreach (var currentDishIngredient in dishIngredientsDict[dishName])                                //dla każdego składnika dania na liście składników dania
                    {
                        bool isBaseIngredient = IsBaseIngredient(currentDishIngredient, baseIngredientsList);           //sprawdź czy składnik jest podstawowym składnikiem
                        if (isBaseIngredient)                                                                           //jeśli jest, dodaj do listy podstawowych składników tego dania
                        {
                            dishBasicIngredientsList.Add(currentDishIngredient);
                        }
                        else                                                                                            //jeśli nie, wyszukaj zagnieżdżone danie na liście dań
                        {
                            foreach (var dishFromList in dishList)
                            {
                                if (dishFromList == currentDishIngredient)                                              //jeśli jest na liście dań
                                {
                                    string[] insideDishIngredients = Convert(
                                        dishFromList, baseIngredientsList, dishIngredientsDict, dishList);              //konwertuj zagnieżdżone danie na podstawowe skłądniki

                                    foreach (var insideDish in insideDishIngredients)
                                        dishBasicIngredientsList.Add(insideDish);                                       //dodaj wszystkie składniki zagnieżdżonego dania na listę składników dania
                                }
                            }
                        }
                    }
                }
            }
            return dishBasicIngredientsList.ToArray();
        }

        private bool IsBaseIngredient(string givenIngredient, BasicIngredient[] baseIngredientsList)
        {
            for (int i = 0; i < baseIngredientsList.Length; i++)
                if (givenIngredient == baseIngredientsList[i].ingredientName) return true;

            return false;
        }
    }
}
