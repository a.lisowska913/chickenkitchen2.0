﻿using System.Collections.Generic;
using ChickenKitchen.Data.Dishes.BasicIngredients;

namespace ChickenKitchen.Data.Dishes
{
    public class CreateDishPriceList
    {
        public Dictionary<string, int> Create(
            BasicIngredient[] basicIngredientsList,
            Dictionary<string, string[]> DishToIngredientsDict)
        {
            Dictionary<string, int> dishPrice = new Dictionary<string, int>();
            foreach (KeyValuePair<string, string[]> dish in DishToIngredientsDict)
            {
                dishPrice.Add(dish.Key, 0);
                foreach (string dishIngredient in DishToIngredientsDict[dish.Key])
                {
                    for (int i = 0; i < basicIngredientsList.Length; i++)
                        if (dishIngredient == basicIngredientsList[i].ingredientName) dishPrice[dish.Key] += basicIngredientsList[i].ingredientPrice;
                }
            }
            return dishPrice;
        }
    }
}
