﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChickenKitchen.Data.Dishes.BasicIngredients
{
    public class BasicIngredient
    {
        public string ingredientName;
        public int ingredientPrice;

        public BasicIngredient(string ingredientName, int ingredientPrice)
        {
            this.ingredientName = ingredientName;
            this.ingredientPrice = ingredientPrice;
        }
    }
}
