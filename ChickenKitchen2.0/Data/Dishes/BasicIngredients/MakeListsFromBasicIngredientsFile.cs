﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace ChickenKitchen.Data.Dishes
{
    public class MakeListsFromBasicIngredientsFile
    {
        string[] basicIngredientsFile;
        public MakeListsFromBasicIngredientsFile(string[] baseIngredientsFile)
        {
            this.basicIngredientsFile = baseIngredientsFile;
        }
        public List<string> MakeBaseIngredientsList()
        {
            Regex regex = new Regex(@"(\D*): (\D*)");
            List<string> baseIngredientsList = new List<string>();
            foreach (var line in basicIngredientsFile)
                baseIngredientsList.Add(regex.Match(line).Groups[1].Value.Trim());

            return baseIngredientsList;
        }
        public List<int> MakeBaseIngredientsPriceList()
        {
            Regex regex = new Regex(@"(\D*): (\d+)");
            List<int> baseIngredientsPriceList = new List<int>();
            foreach (var line in basicIngredientsFile)
                baseIngredientsPriceList.Add(Int32.Parse(regex.Match(line).Groups[2].Value));

            return baseIngredientsPriceList;
        }
    }
}
