﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChickenKitchen.Data.Dishes.BasicIngredients
{
    public class CreateBasicIngredientList
    {
        public List<BasicIngredient> CreateListOfBasicIngredients()
        {
            MakeListsFromBasicIngredientsFile madeBasicIngredientsList = new MakeListsFromBasicIngredientsFileFactory().Create();
            string[] baseIngredientsList = madeBasicIngredientsList.MakeBaseIngredientsList().ToArray();
            int[] baseIngredientsPriceList = madeBasicIngredientsList.MakeBaseIngredientsPriceList().ToArray();
            List <BasicIngredient> basicIngredients = new List <BasicIngredient>();
            for (int i = 0; i < baseIngredientsList.Length; i++)
            {
                basicIngredients.Add(new BasicIngredient(baseIngredientsList[i], baseIngredientsPriceList[i]));
            }
            return basicIngredients;
        }
    }
}
