﻿using ChickenKitchen.FileOperations;

namespace ChickenKitchen.Data.Dishes
{
    public class MakeListsFromBasicIngredientsFileFactory
    {
        public MakeListsFromBasicIngredientsFile Create()
        {
            ReadFromFile readFromFile = new ReadFromFile();
            string[] baseIngredientsFile = readFromFile.ReadFromBaseIngredientsFile();
            return new MakeListsFromBasicIngredientsFile(baseIngredientsFile);
        }
    }
}
