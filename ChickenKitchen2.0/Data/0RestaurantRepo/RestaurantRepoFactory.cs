﻿using System.Collections.Generic;
using System.Linq;
using ChickenKitchen.Data.ClientData.ClientClassOperations;
using ChickenKitchen.Data.Dishes;
using ChickenKitchen.Data.Dishes.BasicIngredients;

namespace ChickenKitchen.Data
{
    public class RestaurantRepoFactory
    {
        public RestaurantRepo CreateRestaurantRepo()
        {
            BasicIngredient[] basicIngredientsList;
            Dish[] dishList;
            Client[] clientList;

            basicIngredientsList = new CreateBasicIngredientList().CreateListOfBasicIngredients().ToArray();
            dishList = new CreateDishDataList().CreateList(basicIngredientsList).ToArray();
            clientList = new CreateClientList().Create().ToArray();

            return new RestaurantRepo(dishList, basicIngredientsList, clientList);
        }

        public RestaurantRepo CreateRestaurantRepo(Dish[] dishList, BasicIngredient[] basicIngredientsList, Client[] clientList)
        {
            return new RestaurantRepo(dishList, basicIngredientsList, clientList);
        }
    }
}
