﻿using ChickenKitchen.Data.ClientData.ClientClassOperations;
using ChickenKitchen.Data.Dishes;
using ChickenKitchen.Data.Dishes.BasicIngredients;

namespace ChickenKitchen.Data
{
    public class RestaurantRepo
    {
        public BasicIngredient[] basicIngredientsList { get; set; }
        public Dish[] dishList { get; set; }
        public Client[] clientList { get; set; }

        public RestaurantRepo(Dish[] dishList, BasicIngredient[] basicIngredientsList, Client[] clientList)
        {
            this.dishList = dishList;
            this.basicIngredientsList = basicIngredientsList;
            this.clientList = clientList;
        }
    }
}
