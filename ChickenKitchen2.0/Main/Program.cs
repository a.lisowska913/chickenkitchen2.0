﻿using System;
using ChickenKitchen.Data;
using ChickenKitchen.Main.Operations;

namespace ChickenKitchen.Main
{ 
    public class Program
    {
        static void Main(string[] args)
        {
            RestaurantRepo agregatedLists = new RestaurantRepoFactory().CreateRestaurantRepo();

            while (true)
            {
                Console.WriteLine("Kto zamawia?");
                string clientName = Console.ReadLine();
                Console.WriteLine("Co zamawia?");
                string clientOrder = Console.ReadLine();

                string allergyIngredient = new Order().CheckAllergies(agregatedLists, clientName, clientOrder);

                if (allergyIngredient != "none") Console.WriteLine(clientName + " nie może zamówić dania. Ma alergię na " + allergyIngredient);
                else Console.WriteLine("Zamówienie " + clientName + " się powiodło.");
                Console.ReadLine();
            }
        }

    }
}
