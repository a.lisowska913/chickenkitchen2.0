﻿using System.Collections.Generic;
using System.Linq;
using ChickenKitchen.Data;
using ChickenKitchen.Data.ClientData;

namespace ChickenKitchen.Main.Operations
{
    public class Order
    {
        public string CheckAllergies(RestaurantRepo restaurantRepo, string clientName, string clientOrder)
        {
            List<string> clientAllergies = new List<string>();
            for (int i = 0; i < restaurantRepo.clientList.Length; i++)
            {
                if (restaurantRepo.clientList[i].clientName == clientName)
                    clientAllergies = restaurantRepo.clientList[i].clientAllergies.ToList();
            }

            for (int i=0; i< restaurantRepo.dishList.Length; i++)
            {
                if (restaurantRepo.dishList[i].dishName == clientOrder)
                {
                    foreach (var ingredient in restaurantRepo.dishList[i].disBasicIngredients)
                    {
                        foreach (var allergy in clientAllergies)
                        {
                            if (ingredient == allergy) return allergy;
                        }
                    }
                }
            }
            return "none";
        }

        public void ComparePriceToClientFunds(RestaurantRepo restaurantRepo, string clientName, string clientOrder)
        {


        }
    }
}
